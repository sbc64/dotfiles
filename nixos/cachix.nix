# WARN: this file will get overwritten by $ cachix use <name>
{ pkgs, lib, ... }:
{
  nix = {
    binaryCaches = [
      "https://cache.nixos.org/"
      "https://walletconnect.cachix.org"
      "https://nix-community.cachix.org"
    ];
    binaryCachePublicKeys = [
      "walletconnect.cachix.org-1:gOjJFP3ijKWCpRP4Oax2IWxK8nCLJIt047NCBMtMYNQ="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };
}
