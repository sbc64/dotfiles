{
  config,
  pkgs,
  lib,
  ...
}: {
  # enable to allow wireguard to capture ::/0
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./cachix.nix
  ];
  systemd.services."erigon" = {
    enable = false;
    wantedBy = ["multi-user.target"];
    serviceConfig = {
      Restart = "on-failure";
      ExecStart = lib.strings.concatStringsSep " " ["${pkgs.erigon}/bin/erigon" "--datadir=/nix/blockchains/erigon"];
      DynamicUser = "yes";
    };
  };

  boot = {
    # Needed for pi3 iso img
    binfmt.emulatedSystems = ["aarch64-linux"];
    #kernelPackages = pkgs.linuxPackages_5_15;
    kernelPackages = pkgs.linuxPackages_latest;
    #kernelModules = ["kvm-amd" "kvm" "zfs"];
    kernelModules = ["kvm-amd" "kvm"];
    kernelParams = [
      "amd_iommu=on"
      "ivrs_ioapic[32]=00:14.0"
      "splash"
      "acpi_backlight=native"
      "btusb.enable_autosuspend=n"
    ];
    blacklistedKernelModules = ["nfc" "pn533" "pn533_usb"];
    loader = {
      systemd-boot.enable = true;
      systemd-boot.configurationLimit = 20;
      systemd-boot.consoleMode = "keep";
      timeout = 1;
      efi.canTouchEfiVariables = true;
    };
  };

  networking = {
    hostName = "mini";
    hostId = builtins.substring 0 8 (
      builtins.hashString "md5" config.networking.hostName
    );
    enableIPv6 = true;
    firewall = {
      enable = true;
    };
    networkmanager.enable = true;
    interfaces.enp2s0.useDHCP = true;
    interfaces.wlp4s0.useDHCP = true;
    #defaultGateway.address = "172.33.12.254";
    extraHosts = "127.0.0.1 localhost";
    wireguard.enable = true;
  };
  # Set your time zone.
  time.timeZone = "Europe/London";
  services.trezord.enable = false;
  services.gnome.gnome-keyring.enable = true;
  services.ntp.enable = true;
  programs = {
    #seahorse.enable = true;
    noisetorch.enable = true;
    zsh = {
      enable = true;
      enableCompletion = true;
      autosuggestions = {
        enable = true;
        highlightStyle = "fg=6";
        strategy = ["history" "completion" "match_prev_cmd"];
      };
    };
  };

  environment.pathsToLink = ["/share/zsh"];
  environment.systemPackages = with pkgs; [
    mullvad-vpn
    libfaketime
    fakeroot
    glibc
    nmap
    pciutils
    lsof
    wireguard-tools
    acpi
    wget
    curl
    nmap
    traceroute
    bind
    # provides dig
    ldns
    # provides drill
    less
    # better than the busybox less
    htop
    git
    binutils
    gcc
    gnumake
    gnupg
    openssl
    vim
    xorg.xbacklight
    libnotify
    glib
    alsaLib
    alsaUtils
    gtk2
    libnfc
    xorg.xmodmap
  ];
  # Enable sound.
  sound = {
    enable = true;
    mediaKeys.enable = true;
  };
  hardware = {
    bluetooth = {
      enable = true;
      package = pkgs.bluezFull;
    };
    # https://github.com/google/security-research/security/advisories/GHSA-h637-c88j-47wq
    bluetooth.settings.General.Enable = "Source,Sink,Media,Socket";
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };
    enableRedistributableFirmware = true;
    acpilight.enable = true;
    ledger.enable = true;
    cpu.amd.updateMicrocode = true;
  };
  fonts = {
    fonts = with pkgs; [
      corefonts
      nerdfonts #really large
      iosevka
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      noto-fonts-extra
      xkcd-font
      open-sans
      liberation_ttf
      fira-code
      fira-code-symbols
      symbola
      google-fonts
      hack-font
      liberation_ttf
      freefont_ttf
      dina-font
      proggyfonts
      dejavu_fonts
      fira
      fira-mono
      font-awesome
      inconsolata
      emojione
      twemoji-color-font
      twitter-color-emoji
    ];
    fontconfig = {
      defaultFonts = {
        serif = ["NotoSerif"];
        sansSerif = ["NotoSans"];
        monospace = ["FiraCode"];
      };
    };
  };

  security.rtkit.enable = true;
  #Whether to enable the RealtimeKit system service, which hands out realtime scheduling priority to user processes on demand. For example, the PulseAudio server uses this to acquire realtime priority.
  services = {
    ipfs.enable = false;
    mullvad-vpn.enable = true;
    pipewire = {
      enable = true;
      alsa = {
        enable = false;
        support32Bit = true;
      };
      jack.enable = false;
      pulse.enable = true;
      config.pipewire = {
        "context" = {
          "modules" = [
            {name = "libpipewire-module-client-node";}
            {name = "libpipewire-module-client-device";}
            {name = "libpipewire-module-echo-cancel";}
          ];
        };
      };
    };
    neard.enable = false; #nfc
    pcscd = {
      enable = true;
      plugins = [pkgs.ccid];
    };
    udev = {
      packages = [pkgs.yubikey-personalization pkgs.libu2f-host];
      # disables builtin bluetooth
      extraRules = ''
        # This rule is needed for basic functionality of the controller in Steam and keyboard/mouse emulation
        SUBSYSTEM=="usb", ATTRS{idVendor}=="28de", MODE="0666"

        # This rule is necessary for gamepad emulation;
        KERNEL=="uinput", MODE="0660", USER="sebas", OPTIONS+="static_node=uinput"

        # Valve HID devices over USB hidraw
        KERNEL=="hidraw*", ATTRS{idVendor}=="28de", MODE="0666"

        # Valve HID devices over bluetooth hidraw
        KERNEL=="hidraw*", KERNELS=="*28DE:*", MODE="0666"

        SUBSYSTEM=="usb", ATTRS{idVendor}=="8087", ATTRS{idProduct}=="0025", ATTR{authorized}="1"
        # Ultimate Hacking Keyboard rules
        SUBSYSTEM=="input", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="612[0-7]", GROUP="input", MODE="0660"
        SUBSYSTEMS=="usb", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="612[0-7]", TAG+="uaccess"
        KERNEL=="hidraw*", ATTRS{idVendor}=="1d50", ATTRS{idProduct}=="612[0-7]", TAG+="uaccess"
        SUBSYSTEM=="usb", ATTR{idVendor}=="091e", ATTRS{idProduct}=="4c29" MODE="0666", GROUP="uucp"
      '';
    };
    printing = {
      enable = false;
      drivers = with pkgs; [gutenprint];
    };
    # TLP power management
    tlp = {
      enable = false; # too long to shutdown, bluetooth breaks
      settings = {
        WIFI_PWR_ON_BAT = "off";
        USB_EXCLUDE_BTUSB = "1";
      };
    };
    # eabling this makes the usb hub stop working
    #usbguard.enable = true;
    # Enable the X11 windowing system.
    xserver = {
      enable = true;
      layout = "us";
      videoDrivers = ["amdgpu"];
      # caps locks boot
      xkbOptions = "ctrl:nocaps,compose:ralt";
      autorun = true;
      # Enable touchpad support.
      libinput = {
        enable = true;
        touchpad = {
          tapping = true;
          tappingDragLock = false;
          accelSpeed = "0.1";
        };
      };
      synaptics = {
        dev = "/dev/input/event12";
        accelFactor = "1";
        minSpeed = "0.5";
        maxSpeed = "1.0";
        scrollDelta = 600;
      };
      desktopManager = {
        plasma5 = {
          enable = true;
          runUsingSystemd = true;
        };
      };
    };
    illum.enable = true;
  };
  hardware.trackpoint = {
    enable = true;
    sensitivity = 16;
    speed = 32;
  };
  users = {
    defaultUserShell = pkgs.zsh;
    users.sebas = {
      isNormalUser = true;
      extraGroups = [
        "wheel"
        "audio"
        "sound"
        "video"
        "networkmanager"
        "docker"
        "plugdev"
        "adbusers"
        "jackaudio"
        "kvm"
        "vboxusers"
        "libvirtd"
      ];
    };
  };
  virtualisation.docker = {
    enable = true;
    daemon.settings = {
      bip = "10.23.23.1/24";
      #fixed-cidr = "10.1.1.0/24";
      default-address-pools = [
        {
          base = "172.30.0.0/16";
          size = 24;
        }
        {
          base = "172.31.0.0/16";
          size = 24;
        }
      ];
    };
  };
  system.stateVersion = "22.05";
}
