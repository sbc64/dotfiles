#!/usr/bin/env node

// This script is used to keep me veering off track
// by closing any tabs not in my whitelist during specific
// conditions, specifically:
// - If /tmp/ultra_focus exists
// - If it's not either Saturday or not from the hours of 3pm to 8pm.
//   (I try to relegate browsing/etc. to those hours and keep the rest
//    reserved for intentional work or rest).

const { List, Close } = require("chrome-remote-interface");
const { existsSync, rmSync, statSync, readFileSync } = require("fs");
const { exec } = require("child_process");

process.title = "whitelist";

const whitelist = JSON.parse(readFileSync(process.argv[2], "utf-8"));

const matchesWhiteList = (str) => whitelist.some((x) => str.includes(x));

const log = (params) => {
  const date = new Date();
  const time =
    date.getHours() +
    ":" +
    date.getMinutes() +
    ":" +
    date.getSeconds() +
    "." +
    +date.getMilliseconds();
  console.log(time, ...params);
};

log(["Starting browser whitelist"]);

const host = "127.0.0.1";

function isTimerActive() {
  let exception = "/tmp/make_focus_exception";
  if (existsSync(exception)) {
    let modified = new Date(statSync(exception).mtime);
    if ((new Date() - modified) / (1000 * 60) > 1) {
      rmSync(exception);
    } else {
      return false;
    }
  }
  let date = new Date();
  return (
    date.getHours() < 18 ||
    date.getHours() >= 20 ||
    existsSync("/tmp/ultra_focus")
  );
}

const browsers = [
  { port: 9222, conn: false },
  { port: 9223, conn: false },
  { port: 9224, conn: false },
  { port: 9225, conn: false },
];

function wrapped_pkill(p) {
  try {
    exec(`pkill ${p}`);
  } catch (e) {}
}

const main = async (browsers) => {
  let port = browsers.port;
  try {
    if (port == 9222) {
      wrapped_pkill("signal");
      wrapped_pkill("firefox");
    }
    log(["Port", port]);
    let results = await List({ port });
    if (!isTimerActive()) return;
    results = results
      .filter((target) => target.type === "page")
      .filter((target) => !matchesWhiteList(target.url));
    for (const tab of results) {
      log(["Shutting down", tab.url]);
      console.log(`Closing tab "${tab.title}"`);
      await Close({ port, id: tab.id });
    }
  } catch (e) {
    let acceptableError =
      e.toString().includes("currentWindowGlobal") || e.code === "ECONNREFUSED";
    if (!acceptableError) throw e;
    browsers.conn = false;
  }
};

for (let b of browsers) {
  setInterval(async () => {
    await main(b);
  }, 1000);
}
