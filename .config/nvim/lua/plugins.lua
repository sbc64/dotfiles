require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  -- ui
 use {
    'euclio/vim-markdown-composer',
    rtp = "target/release",
    run = "nix run nixpkgs#cargo -- build --release --locked",
    ft = "markdown"
  }

  use 'neilhwatson/vim_cf3'
  use 'ray-x/go.nvim'
  use 'ray-x/guihua.lua'
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      'nvim-lua/plenary.nvim',
    } 
  }
  use {'nvim-telescope/telescope-ui-select.nvim' }
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make'}

  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons' }
  }

  use {
    'glacambre/firenvim',
    run = function() vim.fn['firenvim#install'](0) end 
  }


  -- completetion
  use "neovim/nvim-lspconfig"
  use "onsails/lspkind.nvim"

  use 'hrsh7th/nvim-cmp'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use "hrsh7th/cmp-nvim-lsp-signature-help"

  use 'L3MON4D3/LuaSnip'
  use 'saadparwaiz1/cmp_luasnip'

  use {
    "zbirenbaum/copilot-cmp",
    module = "copilot_cmp",
  }

  use 'windwp/nvim-autopairs'
  use 'github/copilot.vim'

  -- Ts
  use "jose-elias-alvarez/null-ls.nvim"
  use "jose-elias-alvarez/typescript.nvim"
  -- Rust
  use 'simrat39/rust-tools.nvim'

  use 'hashivim/vim-terraform'

  -- Debugger
  use 'mfussenegger/nvim-dap'

end)

require("telescope").setup {
  extensions = {
    ["ui-select"] = {
      require("telescope.themes").get_dropdown {
      }
    }
  }
}
require("telescope").load_extension("ui-select")

vim.cmd [[
  nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
  nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
  nnoremap <leader>fs <cmd>lua require('telescope.builtin').live_grep()<cr>
]]
  --nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
  --nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>

function getHostname()
    local f = io.popen ("hostname")
    local hostname = f:read("*a") or ""
    f.close()
    hostname = string.gsub(hostname, "\n$", "")
    return hostname
end

if getHostname() == "mini" then
  vim.g.markdown_composer_browser = 'chromium'
end

